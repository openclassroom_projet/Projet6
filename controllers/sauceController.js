const Sauce = require('../models/sauceModel');
const auth = require('../middleware/auth');
const fs = require('fs');

exports.createSauce = (req, res, next) => {
	const sauceObject = JSON.parse(req.body.sauce);

	delete sauceObject._id;
	delete sauceObject._userId;
	const sauce = new Sauce({
		...sauceObject,
		userId : req.auth.userId,
       	imageUrl : `${req.protocol}://${req.get('host')}/images/${req.file.filename}`,
       	likes : 0,
       	dislikes : 0,
       	usersLiked : [],
       	usersDisliked : []
	});

	sauce.save()
   .then(() => { res.status(201).json({message : 'Objet enregistré !'})})
   .catch(error => { res.status(400).json( { error })})
};

exports.allSauce = (req, res, next) => {
	Sauce.find()
	.then( (sauces) => {
		res.status(200).json(sauces);
	})
	.catch( (error) =>{
		res.status(400).json({ error : error });
	});
};

exports.oneSauce = (req, res, next) => {
	Sauce.findOne({ _id : req.params.id })
	.then( (sauces) => {
		res.status(200).json(sauces);
	})
	.catch( (error) =>{
		res.status(400).json({ error : error });
	});
};

exports.modifySauce = (req, res, next) => {
	const sauceObject = req.file ? {
		...JSON.parse(req.body.sauce),
		imageUrl : `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
	} : { ...req.body };
	delete sauceObject._userId;
	Sauce.findOne({ _id : req.params.id })
	.then( (sauce) => {
		if(sauce.userId != req.auth.userId){
			res.status(401).json({ message : 'Non-autorisé' });
		}
		else {
			Sauce.updateOne({ _id : req.params.id }, { ...sauceObject, _id : req.params.id })
			.then( () => res.status(200).json({ message : 'Sauce modifiée!' }))
			.catch( (error) => {
				res.status(400).json({ error : error });
			});
		}
	})
	.catch( (error) => {
		res.status(400).json({ error : error });
	});
};

exports.deleteSauce = (req, res, next) => {
	Sauce.findOne({ _id : req.params.id })
	.then( (sauce) => {
		if(sauce.userId != req.auth.userId){
			res.status(401).json({ message : 'Non-autorisé' });
		}
		else {
			const filename = sauce.imageUrl.split('/images/')[1];
			fs.unlink(`images/${filename}`, () => {
				Sauce.deleteOne({ _id : req.params.id })
				.then( () => { 
					res.status(200).json({ message : 'Sauce supprimé !'});
				})
				.catch( (error) => {
					res.status(401).json({ error : error });
				});
			});
		}
	})
	.catch( (error) => {
		res.status(500).json({ error : error });
	});
};

exports.likeSauce = (req, res, next) => {
	
	Sauce.findOne({ _id : req.params.id })
	.then( (sauces) => {
		
		let sauceObject = {
			_id : req.params.id,
			userId : sauces.userId,
			name : sauces.name,
			manufacturer : sauces.manufacturer,
			description : sauces.description,
			mainPepper : sauces.mainPepper,
			imageUrl : sauces.imageUrl,
			heat : sauces.heat,
			likes : sauces.likes,
			dislikes : sauces.dislikes,
			usersLiked : sauces.usersLiked,
			usersDisliked : sauces.usersDisliked
		};
		const inLikeArray = sauces.usersLiked.findIndex(user => user == req.auth.userId);
		const inDislikeArray = sauces.usersDisliked.findIndex(user => user == req.auth.userId);
		const reqLike = req.body.like;
		let likeSauce = sauces.likes;
		let dislikeSauce = sauces.dislikes;
		if(inLikeArray == -1){
			if(inDislikeArray == -1){
				if(reqLike == 1){

					sauceObject.usersLiked.push(req.auth.userId);
					sauceObject.likes = sauceObject.likes+1;
					
					Sauce.updateOne({ _id : req.params.id }, { ...sauceObject })
					.then( () => res.status(200).json({ message : 'Sauce modifiée!' }))
					.catch( (error) => {
						res.status(400).json({ error : error });
					});
				}
				else if(reqLike == -1){

					sauceObject.usersDisliked.push(req.auth.userId);
					sauceObject.dislikes = sauceObject.dislikes+1;
					Sauce.updateOne({ _id : req.params.id }, { ...sauceObject })
					.then( () => res.status(200).json({ message : 'Sauce modifiée!' }))
					.catch( (error) => {
						res.status(400).json({ error : error });
					});
				}
			}
			else{
				if(reqLike == 1){

					sauceObject.usersDisliked.splice(inDislikeArray, 1);
					sauceObject.usersLiked.push(req.auth.userId);
					sauceObject.likes = sauceObject.likes+1;
					sauceObject.dislikes = sauceObject.dislikes-1;
					Sauce.updateOne({ _id : req.params.id }, { ...sauceObject })
					.then( () => res.status(200).json({ message : 'Sauce modifiée!' }))
					.catch( (error) => {
						res.status(400).json({ error : error });
					});
				}
				else if(reqLike == 0){

					sauceObject.usersDisliked.splice(inDislikeArray, 1);
					sauceObject.dislikes = sauceObject.dislikes-1;
					Sauce.updateOne({ _id : req.params.id }, { ...sauceObject })
					.then( () => res.status(200).json({ message : 'Sauce modifiée!' }))
					.catch( (error) => {
						res.status(400).json({ error : error });
					});
				}
				else if(reqLike == -1){
					res.status(401).json({ message : 'Non-autorisé' });
				}
			}
		}
		else{
			if(reqLike == 1){
				res.status(401).json({ message : 'Non-autorisé' });
			}
			else if(reqLike == 0){

				sauceObject.usersLiked.splice(inLikeArray, 1);
				sauceObject.likes = sauceObject.likes-1;
				Sauce.updateOne({ _id : req.params.id }, { ...sauceObject })
				.then( () => res.status(200).json({ message : 'Sauce modifiée!' }))
				.catch( (error) => {
					res.status(400).json({ error : error });
				});
			}
			else if(reqLike == -1){
				
				sauceObject.usersDisliked.push(req.auth.userId);
				sauceObject.usersLiked.splice(inLikeArray, 1);
				sauceObject.likes = sauceObject.likes-1;
				sauceObject.dislikes = sauceObject.dislikes+1;
				Sauce.updateOne({ _id : req.params.id }, { ...sauceObject })
				.then( () => res.status(200).json({ message : 'Sauce modifiée!' }))
				.catch( (error) => {
					res.status(400).json({ error : error });
				});
			}
		}
	})
	.catch( (error) => {
		res.status(400).json({ error : error });
	});
};
