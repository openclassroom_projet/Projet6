const express = require('express');
const usetCtrl = require('../controllers/userController');
const router = express.Router();

router.post('/signup', usetCtrl.signup);
router.post('/login', usetCtrl.login);

module.exports = router;